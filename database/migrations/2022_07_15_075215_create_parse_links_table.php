<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParseLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parse_links', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('parent_id')->nullable();
            $table->string('url', 255);
            $table->tinyInteger('level')
                ->unsigned()
                ->comment("1 - base url; 2 - category url; 3 - item url");
            $table->tinyInteger('status')
                ->unsigned()
                ->comment("1 - new; 2 - in work; 3 - done");
            $table->timestamps();

            $table->unique('url');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parse_links');
    }
}
