<?php

namespace App\Console\Commands;

use App\Models\ParseLink;
use Illuminate\Console\Command;
use GuzzleHttp\Client;
use DiDom\Document;
use App\Jobs\StartParse;

class parse extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse https://www.kreuzwort-raetsel.net/';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
//    public function handle()
//    {
//        $arr = [
//            'https://www.kreuzwort-raetsel.net/a',
//            'https://www.kreuzwort-raetsel.net/b',
//            'https://www.kreuzwort-raetsel.net/c',
//            'https://www.kreuzwort-raetsel.net/d',
//            'https://www.kreuzwort-raetsel.net/e',
//            'https://www.kreuzwort-raetsel.net/f',
//            'https://www.kreuzwort-raetsel.net/g',
//            'https://www.kreuzwort-raetsel.net/h',
//            'https://www.kreuzwort-raetsel.net/i',
//            'https://www.kreuzwort-raetsel.net/j',
//            'https://www.kreuzwort-raetsel.net/k',
//            'https://www.kreuzwort-raetsel.net/l',
//            'https://www.kreuzwort-raetsel.net/m',
//            'https://www.kreuzwort-raetsel.net/n',
//            'https://www.kreuzwort-raetsel.net/o',
//            'https://www.kreuzwort-raetsel.net/p',
//            'https://www.kreuzwort-raetsel.net/q',
//            'https://www.kreuzwort-raetsel.net/r',
//            'https://www.kreuzwort-raetsel.net/s',
//            'https://www.kreuzwort-raetsel.net/t',
//            'https://www.kreuzwort-raetsel.net/u',
//            'https://www.kreuzwort-raetsel.net/v',
//            'https://www.kreuzwort-raetsel.net/w',
//            'https://www.kreuzwort-raetsel.net/x',
//            'https://www.kreuzwort-raetsel.net/y',
//            'https://www.kreuzwort-raetsel.net/z',
//            'https://www.kreuzwort-raetsel.net/sonstige',
//        ];
//
//        self::startMultiProcess($arr, function($link, $level = 2) {
//            if(!ParseLink::checkUrlIsset($link)) {
//                $startLink = new ParseLink([
//                    "url" => $link,
//                    "level" => $level,
//                    "status" => ParseLink::PARSE_LINK_STATUS_NEW,
//                ]);
//                $startLink->save();
//
//                StartParse::dispatch($startLink->id);
//            }
//        },
//        2,
//        0);
//    }

    private static function getPageData($url, $level)
    {
        $client = new Client();
//        $response = $client->request('GET', $url, ['proxy' => 'http://195.158.3.198:3128']);
        $response = $client->request('GET', $url);
        $html = $response->getBody();
        $document = new Document();
        $document->loadHtml($html);
        $items = $document->find('ul.dnrg a');

        foreach ($items as $key => $item) {
            $items[$key] = env('PARSER_BASE_URL') . $item->attributes()['href'];
        }

        return $items;
    }

//    public function handle()
//    {
//        $startLink = ParseLink::where('url', env('PARSER_START_LINK'))
//            ->first();
//
//        if ($startLink && $startLink->status == 3) {
//            return $this->info("Parsing from start url: " . env('PARSER_START_LINK') . " is finished");
//        } elseif (!$startLink) {
//            $startLink = new ParseLink([
//                "url" => env('PARSER_START_LINK'),
//                "level" => ParseLink::PARSE_LINK_LEVEL_ONE,
//                "status" => ParseLink::PARSE_LINK_STATUS_NEW,
//            ]);
//            $startLink->save();
//
//            $urlData = self::getPageData($startLink->url, $startLink->level);
//
//            self::startMultiProcess(
//                $urlData,
//                function($link, $level = 2, $parentId) {
//                    if(!ParseLink::checkUrlIsset($link)) {
//                        $startLink = new ParseLink([
//                            "url" => $link,
//                            "level" => $level,
//                            "status" => ParseLink::PARSE_LINK_STATUS_NEW,
//                            "parent_id" => $parentId,
//                        ]);
//                        $startLink->save();
//                    }
//                },
//                $startLink->level + 1,
//                $startLink->id
//            );
//        }
//    }

    public function handle()
    {
        $startLink = ParseLink::where('url', env('PARSER_START_LINK'))
            ->first();

        if ($startLink && $startLink->status == 3) {
            return $this->info("Parsing from start url: " . env('PARSER_START_LINK') . " is finished");
        } elseif (!$startLink) {
            $startLink = new ParseLink([
                "url" => env('PARSER_START_LINK'),
                "level" => ParseLink::PARSE_LINK_LEVEL_ONE,
                "status" => ParseLink::PARSE_LINK_STATUS_NEW,
            ]);
            $startLink->save();

            $urlData = self::getPageData($startLink->url, $startLink->level);

            foreach ($urlData as $link) {
                if (!ParseLink::checkUrlIsset($link)) {
                    $link = new ParseLink([
                        "url" => $link,
                        "level" => ParseLink::PARSE_LINK_LEVEL_TWO,
                        "status" => ParseLink::PARSE_LINK_STATUS_NEW,
                        "parent_id" => $startLink->id,
                        ]);
                    $link->save();
                }
            }

            $startLink->status = ParseLink::PARSE_LINK_STATUS_IN_WORK;
            $startLink->save();

            StartParse::dispatch($urlData);
            return $this->info("Start parsing from start url: " . env('PARSER_START_LINK'));
        }
    }
}
