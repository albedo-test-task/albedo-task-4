<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ParseLink extends Model
{
    use HasFactory;

    public const PARSE_LINK_STATUS_NEW = 1;
    public const PARSE_LINK_STATUS_IN_WORK = 2;
    public const PARSE_LINK_STATUS_DONE = 3;

    public const PARSE_LINK_LEVEL_ONE = 1;
    public const PARSE_LINK_LEVEL_TWO = 2;
    public const PARSE_LINK_LEVEL_THREE = 3;

    protected $fillable = [
        'url',
        'level',
        'status',
        'parent_id',
    ];

    /**
     * Return count links in work
     * @return int
     */
    public static function getActiveLinksCount(): int
    {
        return self::where("status", self::PARSE_LINK_STATUS_IN_WORK)
            ->count();
    }

    /**
     * Check if link isset (true - link isset)
     * @param  string  $url
     * @return bool
     */
    public static function checkUrlIsset(string $url): bool
    {
        return self::where('url', '=', $url)->exists();
    }

}
