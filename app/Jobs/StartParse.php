<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\ParseLink;
use Illuminate\Support\Facades\Log;

class StartParse implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $urlsArray;      // Urls array

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($urlsArray)
    {
        $this->urlsArray = $urlsArray;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        self::startMultiProcess(
            $this->urlsArray,
            function($link) {
                $parseLink = ParseLink::firstWhere('url', $link);
                $parseLink->status = ParseLink::PARSE_LINK_STATUS_IN_WORK;

                $parseLink->save();
            });
    }

    /**
     * Multi process job
     *
     * @param  array  $arr
     * @param  callable  $func
     * @return void
     */
    private function startMultiProcess(array $arr, callable $func)
    {
//        Log::info("Test 4");
        $process_number = env('PARSER_NUMBER_OF_THREADS');

        for ($proc_num = 1; $proc_num <= $process_number; ++$proc_num) {
            $pid = pcntl_fork();

            if ($pid == 0) {
                break;
            }
        }

        if ($pid) {
            for ($i = 1; $i <= $process_number; $i++) {
                pcntl_wait($status);
            }

            return;
        }

        $l = count($arr);

        for ($i = $proc_num; $i <= $l; $i += $process_number) {
            $func($arr[$i - 1]);
        }
    }
}
