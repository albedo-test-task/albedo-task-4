<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use App\Models\ParseLink;
use GuzzleHttp\Client;
use DiDom\Document;
use Illuminate\Support\Facades\Log;

class IndexController extends Controller
{
    public function index1()
    {
        $allowedThreads = env('PARSER_NUMBER_OF_THREADS') - ParseLink::getActiveLinksCount();
//        $startLinks = ParseLink::where("status", ParseLink::PARSE_LINK_STATUS_NEW)
//            ->take(env('PARSER_NUMBER_OF_THREADS'))
//            ->get();

        echo "<pre>";
        print_r($allowedThreads);


        die;
        return view('index');
    }

    public function index2()
    {
        $url = "https://www.kreuzwort-raetsel.net/uebersicht.html";
        $baseUri = "https://www.kreuzwort-raetsel.net/";

        $client = new Client();
        $response = $client->request('GET', $url);

//        echo $response->getStatusCode(); // 200
        $html = $response->getBody();

        $document = new Document();
        $document->loadHtml($html);
        $items = $document->find('ul.dnrg a');
echo "<pre>";
        foreach ($items as $item) {
            print_r($baseUri . $item->attributes()['href']);
            echo "<br />";
        }

        die;
    }

    public function index() {
        $arr = [
            'https://www.kreuzwort-raetsel.net/a',
            'https://www.kreuzwort-raetsel.net/b',
            'https://www.kreuzwort-raetsel.net/c',
            'https://www.kreuzwort-raetsel.net/d',
            'https://www.kreuzwort-raetsel.net/e',
            'https://www.kreuzwort-raetsel.net/f',
            'https://www.kreuzwort-raetsel.net/g',
            'https://www.kreuzwort-raetsel.net/h',
            'https://www.kreuzwort-raetsel.net/i',
            'https://www.kreuzwort-raetsel.net/j',
            'https://www.kreuzwort-raetsel.net/k',
            'https://www.kreuzwort-raetsel.net/l',
            'https://www.kreuzwort-raetsel.net/m',
            'https://www.kreuzwort-raetsel.net/n',
            'https://www.kreuzwort-raetsel.net/o',
            'https://www.kreuzwort-raetsel.net/p',
            'https://www.kreuzwort-raetsel.net/q',
            'https://www.kreuzwort-raetsel.net/r',
            'https://www.kreuzwort-raetsel.net/s',
            'https://www.kreuzwort-raetsel.net/t',
            'https://www.kreuzwort-raetsel.net/u',
            'https://www.kreuzwort-raetsel.net/v',
            'https://www.kreuzwort-raetsel.net/w',
            'https://www.kreuzwort-raetsel.net/x',
            'https://www.kreuzwort-raetsel.net/y',
            'https://www.kreuzwort-raetsel.net/z',
            'https://www.kreuzwort-raetsel.net/sonstige',
        ];

        dd($arr);

        die;
    }
}
